const changeBtn = document.querySelector('.change-theme-btn')
console.log(changeBtn);
changeBtn.addEventListener('click', () =>{

    const body = document.querySelector('body')
    if (body.getAttribute('theme') == 'dark') {
        localStorage.setItem('theme', 'white')
        body.setAttribute('theme','white')
    }else{
        localStorage.setItem('theme', 'dark')
        body.setAttribute('theme','dark')
    }
})
if (localStorage.getItem('theme')) {
    let body = document.querySelector('body')
    body.setAttribute('theme', localStorage.getItem('theme'))
}